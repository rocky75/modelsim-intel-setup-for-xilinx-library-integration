#/bin/bash

echo Installing dependencies...
pacaur -S --needed expat fontconfig freetype2 xorg-fonts-type1 glibc gtk2 libcanberra libpng libpng12 libice libsm util-linux ncurses tcl tcllib zlib libx11 libxau libxdmcp libxext libxft libxrender libxt libxtst lib32-expat lib32-fontconfig lib32-freetype2 lib32-glibc lib32-gtk2 lib32-libcanberra lib32-libpng lib32-libpng12 lib32-libice lib32-libsm lib32-util-linux lib32-ncurses lib32-zlib lib32-libx11 lib32-libxau lib32-libxdmcp lib32-libxext lib32-libxft lib32-libxrender lib32-libxt lib32-libxtst ncurses5-compat-libs lib32-ncurses5-compat-libs

echo Running setup file...
./ModelSimSetup-15.1.0.185-linux.run --modelsim_edition modelsim_ase  --mode unattended --unattendedmodeui minimal &
PID=$!
echo Press enter when the setup dialog says \"Setup complete.\"
read -n 1
kill $PID

wget -q --show-progress https://dl.dries007.net/lib32-freetype2-2.5.0.1.tar.xz -O lib32-freetype2-2.5.0.1.tar.xz
tar xf lib32-freetype2-2.5.0.1.tar.xz -C ~/intelFPGA/18.1/modelsim_ase/

wget -q https://archive.archlinux.org/packages/l/lib32-fontconfig/lib32-fontconfig-2.12.6-1-x86_64.pkg.tar.xz
#installare solo le lib in lib32
/usr/bin/fc-cache-32 -frv

cd ~/intelFPGA/18.1/modelsim_ase/
sed -i 's/linux_rh60/linux/' vco
sed -i 's/dir=`dirname "$arg0"`/dir=`dirname "$arg0"`\nexport LD_LIBRARY_PATH=${dir}\/lib32/' vco # adds "export LD_LIBRARY_PATH=${dir}/lib32" after $dir is found.
cat > ~/.local/share/applications/modelsim.desktop <<EOF
[Desktop Entry]
Version=1.0
Name=ModelSim
Comment=ModelSim
Exec=$HOME/intelFPGA/15.1/modelsim_ase/bin/vsim
Icon=applications-electronics
Terminal=true
Type=Application
Categories=Development
EOF

echo "Done, enjoy!"
