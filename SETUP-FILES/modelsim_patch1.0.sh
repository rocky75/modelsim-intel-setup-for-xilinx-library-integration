#/bin/bash

wget -q --show-progress https://dl.dries007.net/lib32-freetype2-2.5.0.1.tar.xz -O lib32-freetype2-2.5.0.1.tar.xz
tar xf lib32-freetype2-2.5.0.1.tar.xz -C ~/intelFPGA/$1/modelsim_ase/

#wget -q https://archive.archlinux.org/packages/l/lib32-fontconfig/lib32-fontconfig-2.12.6-1-x86_64.pkg.tar.xz
#installare solo le lib in lib32
#/usr/bin/fc-cache-32 -frv

cd ~/intelFPGA/$1/modelsim_ase/
sed -i 's/linux_rh60/linux/' vco
sed -i 's/dir=`dirname "$arg0"`/dir=`dirname "$arg0"`\nexport LD_LIBRARY_PATH=${dir}\/lib32/' vco # adds "export LD_LIBRARY_PATH=${dir}/lib32" after $dir is found.
#patch RDM
sed -i 's/exec "$arg0" "$@"/ for arg do\n    shift\n    \[ "$arg" = "-32" \] \&\& continue\n    set -- "$@" "$arg"\n    done\n    for arg do\n    shift\n    \[ "$arg" = "-64" \] \&\& continue\n    set -- "$@" "$arg"\n    done\n   exec "$arg0" "$@"/' vco 
sed -i 's/exec "$arg0" "$@"/ for arg do\n    shift\n    \[ "$arg" = "-novopt" \] \&\& continue\n    set -- "$@" "$arg"\n    done\n    exec "$arg0" "$@"/' vco 

#modifica export LD_LIBRARY_PATH=/usr/bin in "acroread" 