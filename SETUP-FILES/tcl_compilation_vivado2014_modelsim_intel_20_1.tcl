	
	modelsim.verilog.axi_bfm:-quiet
	modelsim.verilog.ieee:-quiet
	modelsim.verilog.simprim:-source +define+XIL_TIMING
	modelsim.verilog.std:-quiet
	modelsim.verilog.synopsys:-quiet
	modelsim.verilog.unisim:-source
	modelsim.verilog.vl:-quiet
	modelsim.verilog.xpm:-sv
	modelsim.vhdl.axi_bfm:-93 -quiet
	modelsim.vhdl.ieee:-93 -quiet
	modelsim.vhdl.simprim:-source -93
	modelsim.vhdl.std:-93 -quiet
	modelsim.vhdl.synopsys:-93 -quiet
	modelsim.vhdl.unisim:-source -93
	modelsim.vhdl.vl:-93 -quiet
	modelsim.vhdl.xpm:

config_compile_simlib -cfgopt {modelsim.verilog.axi_bfm: }
config_compile_simlib -cfgopt {modelsim.verilog.ieee: }
config_compile_simlib -cfgopt {modelsim.verilog.simprim:-source +define+XIL_TIMING}
config_compile_simlib -cfgopt {modelsim.verilog.std: }
config_compile_simlib -cfgopt {modelsim.verilog.synopsys: }
config_compile_simlib -cfgopt {modelsim.verilog.unisim:-source}
config_compile_simlib -cfgopt {modelsim.verilog.vl: }
config_compile_simlib -cfgopt {modelsim.verilog.secureip: }
config_compile_simlib -cfgopt {modelsim.verilog.xpm:-sv}

config_compile_simlib -cfgopt {modelsim.vhdl.axi_bfm:-2008}
config_compile_simlib -cfgopt {modelsim.vhdl.ieee:-2008}
config_compile_simlib -cfgopt {modelsim.vhdl.simprim:-source -2008}
config_compile_simlib -cfgopt {modelsim.vhdl.std:-2008 }
config_compile_simlib -cfgopt {modelsim.vhdl.synopsys:-2008 }
config_compile_simlib -cfgopt {modelsim.vhdl.vl:-2008 }
config_compile_simlib -cfgopt {modelsim.vhdl.unisim:-source -2008 }
config_compile_simlib -cfgopt {modelsim.vhdl.xpm: }

set_param general.maxThreads 8
compile_simlib -language all -dir /home/harlock/intelFPGA/XilixLibs -simulator modelsim -simulator_exec_path /home/harlock/intelFPGA/20.1/modelsim_ase/bin -32bit -verbose -library all -family all